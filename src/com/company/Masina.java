package com.company;

import javax.swing.*;

public class Masina {

    public static String marca;
    public static String model;
    public static int an;

    public static void getMarca() {
        JOptionPane.showMessageDialog(null, marca);
    }

    public static void getModel() {
        JOptionPane.showMessageDialog(null, model);
    }

    public static void getAn() {
        JOptionPane.showMessageDialog(null, an);
    }

    public static void setMarca(String marca) {
        Masina.marca = marca;
    }

    public static void setModel(String model) {
        Masina.model = model;
    }

    public static void setAn(Integer an) {
        Masina.an = an;
    }

    @Override
    public String toString() {
        return "Masina{}";
    }
}
